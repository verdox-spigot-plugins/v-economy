package de.verdox.economy.converter;

import java.io.IOException;
import java.sql.SQLException;

public interface Converter {
    boolean ymlConvert(boolean overwrite) throws IOException;
    boolean sqLiteConvert(boolean overwrite) throws SQLException;
    boolean sqlConverter(boolean overwrite) throws SQLException;

    public enum ConverterType{
        Essentials,
        GemsEconomy,
    }
}
