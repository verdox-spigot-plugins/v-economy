package de.verdox.economy.converter;

import de.verdox.economy.EconomyMain;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class EssentialsConverter implements Converter {
    @Override
    public boolean ymlConvert(boolean overwrite) {
        Path vEconomyFolder = FileSystems.getDefault().getPath(EconomyMain.plugin.getDataFolder().getAbsolutePath());
        Path pluginFolder = vEconomyFolder.getParent();
        Path userData = pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/Essentials/userdata");

        EconomyMain.consoleMessage("&4Conversion started with overwrite = &b"+overwrite);

        if(!pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/Essentials").toFile().exists()){
            EconomyMain.consoleMessage("&4Conversion failed due to now Essentials folder found!");
            return false;
        }

        if(!pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/Essentials").toFile().isDirectory()){
            EconomyMain.consoleMessage("&4Conversion failed due to now Essentials folder found!");
            return false;
        }
        if(!pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/Essentials/userdata").toFile().exists()){
            EconomyMain.consoleMessage("&4Conversion failed due to now Essentials/userdata folder found!");
            return false;
        }
        if(!pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/Essentials/userdata").toFile().isDirectory()){
            EconomyMain.consoleMessage("&4Conversion failed due to now Essentials/userdata folder found!");
            return false;
        }

        File[] files = userData.toFile().listFiles();
        EconomyMain.consoleMessage("&eFound &c"+files.length+" &efiles&7!");
        convertYmlFiles(Arrays.asList(files),200,1,overwrite);
        return true;
    }

    @Override
    public boolean sqLiteConvert(boolean overwrite) throws SQLException {
        return false;
    }

    @Override
    public boolean sqlConverter(boolean overwrite) throws SQLException {
        return false;
    }


    // Inner stuff for yml conversion
    // Recursive Conversiontechnique used!

    private void convertYmlFiles(List<File> files, int stepRange, int delay, boolean overwrite){

        List<File> copy = new ArrayList<>(files);

        for(int i = 0;i<stepRange;i++){
            // Cancel Recursion
            if(files.isEmpty()){
                EconomyMain.consoleMessage("&aConversion successfully finished!");
                return;
            }

            if(i<files.size()){
                File file = files.get(i);
                copy.remove(file);
                convertYmlFile(file,overwrite);
            }
        }
        Bukkit.getScheduler().runTaskLaterAsynchronously(EconomyMain.plugin, () -> {
            convertYmlFiles(copy,stepRange,delay,overwrite);
        },20L*delay);
    }

    private void convertYmlFile(File file, boolean overwrite){
        String fileName = file.getName().replace(".yml","");
        UUID uuid = UUID.fromString(fileName);
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
        YamlConfiguration userDataFile = YamlConfiguration.loadConfiguration(file);
        String userName = userDataFile.getString("lastAccountName");

        if(offlinePlayer==null){
            EconomyMain.consoleMessage("&eOfflinePlayer does not exist&7! &e"+uuid.toString());
            return;
        }

        if(offlinePlayer.getName()!=null)
            userName = offlinePlayer.getName();

        if(!userDataFile.isSet("money")){
            EconomyMain.consoleMessage("&eNo Money found for: &a"+userName);
            return;
        }
        double money = userDataFile.getDouble("money");
        if(userDataFile.isString("money")){
            money = Double.parseDouble(userDataFile.getString("money"));
        }
        if(overwrite == false && EconomyMain.economy.hasAccount(offlinePlayer)){
            EconomyMain.consoleMessage("&e"+userName+" &calready has an account. Skipping! &7[Overwrite = false!]");
        }
        else if(overwrite == true && EconomyMain.economy.hasAccount(offlinePlayer)){
            EconomyMain.economy.withdrawPlayer(offlinePlayer,EconomyMain.economy.getBalance(offlinePlayer));
            EconomyMain.economy.depositPlayer(offlinePlayer,money);
            EconomyMain.consoleMessage("&e"+userName+" &aconverted successfully with Balance: &b"+money);
        }
        else{
            // Is cracked Server?
            if(offlinePlayer.getName() == null){
                EconomyMain.economy.createPlayerAccount(userName,offlinePlayer);
                EconomyMain.economy.depositPlayer(offlinePlayer,money);
            }
            else {
                EconomyMain.economy.createPlayerAccount(offlinePlayer);
                EconomyMain.economy.depositPlayer(offlinePlayer,money);
                EconomyMain.consoleMessage("&e"+userName+" &aconverted successfully with Balance: &b"+money);
            }
        }
    }
}
