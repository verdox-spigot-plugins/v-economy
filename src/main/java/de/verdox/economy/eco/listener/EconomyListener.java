package de.verdox.economy.eco.listener;

import de.verdox.economy.EconomyMain;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EconomyListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        EconomyMain.economy.createPlayerAccount(e.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        EconomyMain.dataConnection.removePlayerFromBalanceCache(e.getPlayer());
    }
}
