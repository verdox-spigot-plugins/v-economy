package de.verdox.economy.eco.model;

public class PlayerBalance {
    private String UUID;
    private Double balance;
    private String name;

    public PlayerBalance(String UUID, Double balance, String name) {
        this.UUID = UUID;
        this.balance = balance;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "PlayerBalance{" +
                "UUID='" + UUID + '\'' +
                ", balance=" + balance +
                ", name='" + name + '\'' +
                '}';
    }
}
