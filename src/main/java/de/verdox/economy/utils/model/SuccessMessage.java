package de.verdox.economy.utils.model;

import de.verdox.economy.utils.configuration.ConfigHandler;

public enum SuccessMessage {
    SUCCESS("&aSuccess!"),
    MONEY_OWNED("&7You currently own&a %money%"),
    PLAYER_OWNS("&7The Player %player% currently owns %money%"),
    SENT_MONEY_TO_PLAYER("&aYou've sent %money% to %player%"),
    RECEIVED_MONEY_FROM_PLAYER("&aYou've received %money% from %player%"),
    TOP_PLAYERS("&3[------ TOP 10 Players ------]"),
    ADMIN_SET_MONEY("&aThe money of the player %player% was set to %money%"),
    ADMIN_GIVE_MONEY("&aYou gave %player% %money%"),
    ADMIN_TAKE_MONEY("&aYou removed %money% from player %player%"),
    ADMIN_RESET("Successfully reset account!"),
    CONVERSION_START("&aConversion started look at the &eServerconsole!"),
;

    private String defaultMsg;
    SuccessMessage(String defaultMsg){
        this.defaultMsg = defaultMsg;
    }

    public String getMsg(){
        return ConfigHandler.getSuccessMessage(this);
    }
    public String getMsg(double amount, String playerName){
        return ConfigHandler.getSuccessMessage(this).replace("%player%",playerName).replace("%money%",String.format("%.2f",amount) + ConfigHandler.getCurrencyType());
    }
    public String getDefaultMsg() {
        return this.defaultMsg;
    }
}

