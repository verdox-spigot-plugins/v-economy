package de.verdox.economy.utils.configuration;

import de.verdox.economy.EconomyMain;
import de.verdox.economy.utils.model.ErrorMessage;
import de.verdox.economy.utils.model.SuccessMessage;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;

public class ConfigHandler {

    public static File config = new File ("plugins/V-Economy", "config.yml");
    public static FileConfiguration configFile = YamlConfiguration.loadConfiguration(config);

    public static void save() {
        try {
            configFile.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createConfigFile () {
        //if(!config.exists()) {
        configFile.options().header("VEconomy Config");

        configFile.addDefault("DB_HOST","localhost");
        configFile.addDefault("DB_USER","root");
        configFile.addDefault("DB_PW","nopw");
        configFile.addDefault("DB_DB","local_db");
        configFile.addDefault("DB_PORT","3306");
        configFile.addDefault("CurrencyType","€");
        configFile.addDefault("CurrencyName","Euro");
        configFile.addDefault("Startmoney",500);
        configFile.addDefault("MaxMoney",1000000);
        configFile.addDefault("MaxPayAmount",1000000);
        configFile.addDefault("DatabasePrefix","server1");
        configFile.addDefault("UseLogs",true);
        configFile.addDefault("DB_SSL",false);

        for(ErrorMessage e : ErrorMessage.values()){
            configFile.addDefault("MESSAGES.ERROR." + e.name(),e.getDefaultMsg());
        }

        for(SuccessMessage e : SuccessMessage.values()){
            configFile.addDefault("MESSAGES.SUCCESS." + e.name(),e.getDefaultMsg());
        }

        configFile.options().copyDefaults(true);
        configFile.options().copyHeader(true);
        save();
        // }
    }

    public static void reloadConfig () {
        configFile = YamlConfiguration.loadConfiguration(config);

        Reader defConfigStream = null;
        try {
            defConfigStream = new InputStreamReader(EconomyMain.getPlugin(EconomyMain.class).getResource("config.yml"), "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (defConfigStream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            configFile.setDefaults(defConfig);
            EconomyMain.consoleMessage("&econfig.yml has been reloaded!");
        }
        save();
    }

    public static int getStartMoney(){return configFile.getInt("Startmoney");}

    public static boolean useSSL(){return configFile.getBoolean("DB_SSL");}

    public static String getCurrencyType(){
        return getString("CurrencyType");
    }

    public static String getCurrencyName(){
        return getString("CurrencyName");
    }

    public static String getString(String key){
        return configFile.getString(key);
    }

    public static boolean useLogs(){return configFile.getBoolean("UseLogs");}

    public static String getSuccessMessage (SuccessMessage successLabel){
        String input = configFile.getString("MESSAGES.SUCCESS." + successLabel.name());
        return ChatColor.translateAlternateColorCodes('&', input);
    }

    public static String getDatabasePrefix(){
        String prefix = getString("DatabasePrefix");
        if(prefix == null || prefix.isEmpty())
            return "server";
        return prefix;
    }

    public static double getMaxMoney(){
        return configFile.getDouble("MaxMoney");
    }

    public static double getMaxPayAmount(){
        return configFile.getDouble("MaxPayAmount");
    }

    public static String getErrorMessage (ErrorMessage errorMessage){
        String input = configFile.getString("MESSAGES.ERROR." + errorMessage.name());
        return ChatColor.translateAlternateColorCodes('&', input);
    }
}
